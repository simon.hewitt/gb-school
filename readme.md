# Light Scultpure

## Project Details

GitLab repo [here](https://gitlab.com/simon.hewitt/gb-school/-/tree/main)  
Google Docs [here](https://drive.google.com/drive/u/1/folders/1hCqhFGqvsXpZjKHnsU59-Fmo2gMGcbRm?ths=true)  
Both should be public

Contacts:

Simon Hewitt - software - simon.d.hewitt@gmail.com
Jeremy Wyatt - Sculptor - jcwyatt42@gmail.com   
Davide Bianchi - Electronics - davidepuntobianchi@gmail.com  





## Mark II

**Arduino**

Tutorials etc:

[random nerd](https://randomnerdtutorials.com/guide-for-ws2812b-addressable-rgb-led-strip-with-arduino/) looks good  
[Official Arduino](inchttps://create.arduino.cc/projecthub/talofer99/arduino-and-addressable-led-b8403f) Very simple  
FastLed [docs](http://fastled.io/docs/3.1/md__r_e_a_d_m_e.html)   
ProMicro pin outs etc [sparkfun](https://learn.sparkfun.com/tutorials/pro-micro--fio-v3-hookup-guide/hardware-overview-pro-micro)   
try [this](https://learn.sparkfun.com/tutorials/pro-micro--fio-v3-hookup-guide/hardware-overview-pro-micro)   

**NOTE** the `pro Micro` I am using does NOT have an on-board LED! (it has `Power`, `TX` and `RX`)

So use pin 7 for an LED and resistor, as it does digital and nothing else.

Pin outs:

|logical pin|physical pin|Capability|Purpose|
|---|---|---|---|
|5|5|PWM|Blue LED|
|6|6|PWM|Yellow LED|
|7|7|-|Adafruit_NeoPixel WS2812|
|19 / A1| Analog input - 0-5V input signal|

**NOTES**

Arduino I have is a `Pro Micro 5V`

WS2812B Wire Colours  
Red 5V  
White 0v  
Green Signal  

 


## Mark I

Does not work - not with RPI 4? The library uses PWM and DMA, I think this works differently on a Pi 4, as it is an entirely different chip.


With `WS2812` LED strips and a Raspberry Pi

A bit fiddly, see [here](https://tutorials-raspberrypi.com/connect-control-raspberry-pi-ws2812-rgb-led-strips/)

Currently can connect via SSH with `ssh pi@192.168.1.144` or `192.168.1.167`.  
passoword is `my top April`

Also the RPI screen works OK! make sure `num lock` is *OFF* on the keyboard (else **i** prints as **5**).

Setup is:

		sudo apt-get update
		sudo apt-get install gcc make build-essential python-dev git scons swig
		sudo vi /etc/modprobe.d/snd-blacklist.conf
		and add the line:
		 blacklist snd_bcm2835
		sudo vi /boot/config.txt
		  and comment out line # dtparam=audio=on
		  (Warning there are several dtparam lines)
		and reboot with `sudo reboot`
		then:
		cd ~/code
		git clone https://github.com/jgarff/rpi_ws281x
		cd rpi_ws281x
		sudo scons
		cd python
		sudo python3 setup.py build 
		sudo python3 setup.py install 
		sudo pip3 install adafruit-circuitpython-neopixel
		
But to get it to work, need this: https://github.com/rpi-ws281x/rpi-ws281x-python/issues/47

And need to run python with sudo, else it cannot open /dev/mem (using DMA I think).

`sudo python3 strandtest.py`

It runs, now need to solder up and see if the LEDs work!  
No they don't.

Simpler path from Adafruit [here](https://learn.adafruit.com/neopixels-on-raspberry-pi/python-usage)

or [here](https://learn.adafruit.com/neopixels-on-raspberry-pi/raspberry-pi-wiring)


		