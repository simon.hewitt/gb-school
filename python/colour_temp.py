#!/usr/bin/env python
"""
Create a list of 3-tuples for approx
colour temperature RGB values.
Simon Hewitt
August 2021
Based on https://stackoverflow.com/questions/25668828/how-to-create-colour-gradient-in-python
The original code was to produce a PNG image.
I have left that in so we can see the colour range produced,
but the main purpose is to produce `map.c` to be included into the Adruino sketch
"""

width, height = 100, 50

import math
from PIL import Image
im = Image.new('RGB', (width, height))
# A map of rgb points in your distribution
# [distance, (r, g, b)]
# distance is percentage from left edge
heatmap = [
    [0.0, (0, 0, 0)],
    [0.20, (0, 0, .5)],
    [0.40, (0, .5, 0)],
    [0.60, (.5, 0, 0)],
    [0.80, (.75, .75, 0)],
    [0.90, (1.0, .75, 0)],
    [1.00, (1.0, 1.0, 1.0)],
]

def gaussian(x, a, b, c, d=0):
    return a * math.exp(-(x - b)**2 / (2 * c**2)) + d

def pixel(x, width=100, map=[], spread=1):
    width = float(width)
    r = sum([gaussian(x, p[1][0], p[0] * width, width/(spread*len(map))) for p in map])
    g = sum([gaussian(x, p[1][1], p[0] * width, width/(spread*len(map))) for p in map])
    b = sum([gaussian(x, p[1][2], p[0] * width, width/(spread*len(map))) for p in map])
    return min(1.0, r), min(1.0, g), min(1.0, b)

spread = 1.1    # Experiment, 1.1 seems to work best
width = im.size[0]
with open('map.c', 'w') as fp:
    fp.write(f'float colour_map [{width}][3] = {{ \n')
    for x in range(width):
        r, g, b = pixel(x, width=im.size[0], map=heatmap, spread=spread)
        r, g, b = [min(int(256*v), 255) for v in (r, g, b)]
        for y in range(im.size[1]):
            im.load()[x, y] = r, g, b
        fp.write(f"{{ {r}, {g}, {b} }}, \n")
    fp.write("};\n")
im.save(f'grad_{str(spread)}.png')