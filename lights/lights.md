# Bedwyn School Power Consumption lights

**October 2011**

## Arduino

Model: Pro Micro, see [Arduino](https://store.arduino.cc/products/arduino-micro), [pinouts](https://www.theengineeringprojects.com/2020/12/introduction-to-arduino-pro-micro.html)

### Pins used:

|Physical Pin|Logical Pin|Name|Purpose|
|---|---|---|---|
|7|7|NEO_LED_PIN |Neo LED control output|
|A0|18|CONTROL_V_IN|0-5V control voltage input|
|1|TX0|MODE_SWITCH_IN|Push switch to pulse between modes|
|5|5|YELLOW_LED|Yellow indicator LED|


### Libraries

Adafruit_NeoPixel [GitHub](https://github.com/adafruit/Adafruit_NeoPixel) -  NeoPixel control library






# General Notes and References

RS Components: Slider pot pinout: [RS COMP](https://docs.rs-online.com/24bf/0900766b80fa58ef.pdf)




