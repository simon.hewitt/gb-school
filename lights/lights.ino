/*
Simon Hewitt
Starting with:
*/

#include <Adafruit_NeoPixel.h>
#include "colour_map.h" // Declares byte colour_map [100][3] {{r,g,b},...}
#include "Arduino.h"
// See python/colour_temp.py for how this is generated

#define DATA_PIN 3
#define YELLOW_LED 5
#define NEO_LED_PIN 7
#define LED_COUNT 170
#define COLOUR_MAP_SIZE 100
#define CONTROL_V_IN A0
#define MODE_SWITCH_IN 1  // The pin labelled TX0 is pin 1 !
#define MAX_STATES 5      // How many states does the push switch cycle between ?
//#define DEBUG 1

#define MAX(a,b) (((a)>(b))?(a):(b))

// Globals and forward decalrations of funcs:
volatile int push_switch_state = 0;

Adafruit_NeoPixel strip(LED_COUNT, NEO_LED_PIN, NEO_GRB + NEO_KHZ800);
// See https://learn.adafruit.com/adafruit-neopixel-uberguide/arduino-library-use

// Colour definitions:
uint32_t greenishwhite = strip.Color(0, 64, 0, 64);
uint32_t magenta = strip.Color(255, 0, 255);
uint32_t white = strip.Color(255, 255, 255);

// +++++++++++  General functions +++++++++++++

void push_switch_isr() {
  // ISR (qv) called when push switch is pushed (ISR pin goes LOW)
  //push_switch_state = ++push_switch_state % MAX_STATES;
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 200) 
  {
    push_switch_state = ++push_switch_state % MAX_STATES;  // Cycle around available states
  }
  last_interrupt_time = interrupt_time;
}

void blink_for(float seconds_delay, int led) {
  // led must be an output pin attached to an LED (or something else?)
  // Flashes on for seconds_delay seconds (which is a float)
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(seconds_delay*1000.0);                       // wait for a second
  digitalWrite(led, LOW);   // turn the LED on (HIGH is the voltage level)
}

int get_control_level() {
  // Returns 0..1023 (includive) based on 0..5V input to CONTRL_V_IN pin (A0)
  return analogRead(CONTROL_V_IN);
}


// +++++++++++  Setup - runs once before loop() +++++++++++++
// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(YELLOW_LED, OUTPUT);
  pinMode(NEO_LED_PIN, OUTPUT);
  pinMode(CONTROL_V_IN, INPUT);
  pinMode(MODE_SWITCH_IN, INPUT_PULLUP);

  Serial.begin(115200);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  strip.setPixelColor(1, 255, 0, 255);
  strip.show(); // Initialize all pixels to 'off'
  attachInterrupt(digitalPinToInterrupt(MODE_SWITCH_IN), push_switch_isr, FALLING);
  Serial.println("Ligh Sculpture V1.0");

}

void show_level(int level, long fill_colour){
  // level 0..1023
  strip.clear();
  int led_level = MAX(int(float(level) / 1023.0 * float(LED_COUNT)), 1);
  strip.fill(fill_colour, 0, led_level);
}

long level_colour(int level) {
  byte r,g,b;
  int x = int(float(level)*float(COLOUR_MAP_SIZE) / 1024.0);

  r = colour_map[x][0];
  g = colour_map[x][1];
  b = colour_map[x][2];

  #ifdef DEBUG
  Serial.print("level: ");
  Serial.print(level);
  Serial.print(" x : ");
  Serial.print(x);
  Serial.print(" rgb : ");
  Serial.print(r);
  Serial.print(g);
  Serial.print(b);
  Serial.println ("--");
  #endif

  return strip.Color(r, g, b);
}

void ripple(int forward_ripple) {
    byte r,g,b;
    static int x = 0;
    static int rotate = 0;
    int map_index;

    rotate = ++rotate % LED_COUNT;

    for(int i=0; i<LED_COUNT; i++) {
      x = (i + rotate) % LED_COUNT;
      map_index = x % COLOUR_MAP_SIZE;
      r = colour_map[map_index][0];
      g = colour_map[map_index][1];
      b = colour_map[map_index][2];
      if (forward_ripple) {
        strip.setPixelColor(i, strip.Color(r, g, b));
        } else {
          strip.setPixelColor(LED_COUNT - i, strip.Color(r, g, b));
       }
      }
}

int flash_by_count(int push_switch_state, int previous_state) {
  // If state has changed, flash that many to show the user. Return the new state
  if (push_switch_state != previous_state) {
    for (int i = 0; i <= push_switch_state; i++) {
      blink_for(0.1, YELLOW_LED);
      delay(150);
    }
    previous_state = push_switch_state;
  }
  return push_switch_state;
}


// the loop function runs over and over again forever
void loop() {
    static byte previous_state = 0;
    int control_level;
    long lc;
    control_level = get_control_level();

    // Serial.println("\nLOOP");
    switch (push_switch_state) {
      case 0:
        ripple(1);
        break;

      case 1:
        ripple(0);
        break;

      case 2:
        lc = level_colour(control_level);
        show_level(control_level, lc);
        break;

      default:
        ripple(1);
    }

  strip.show();


  delay(control_level);

  if (previous_state != push_switch_state) {
    Serial.println("mode: ");
    Serial.println(push_switch_state);
    }

  previous_state = flash_by_count(push_switch_state, previous_state);
}
